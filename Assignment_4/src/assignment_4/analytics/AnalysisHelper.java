/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment_4.analytics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import assignment_4.entities.*;

/**
 *
 * @author ICBC
 */
public class AnalysisHelper {
    
    public void getThreeMostPopularProduct(){
        final Map<Integer, Integer> productQuantityCount = new HashMap<Integer, Integer>();
        Map<Integer, Order> orders = DataStore.getInstance().getOrders();
        for(Order order : orders.values()) {
            int totalQuantity = 0;
            if(productQuantityCount.containsKey(order.getItem().getProductId()))
                totalQuantity = productQuantityCount.get(order.getItem().getProductId());
            totalQuantity += order.getItem().getQuantity();
            productQuantityCount.put(order.getItem().getProductId(), totalQuantity);
        }
        List<Integer> productList = new ArrayList<>(productQuantityCount.keySet());
        Collections.sort(productList, new Comparator<Integer>() {
            @Override
            public int compare(Integer productId1, Integer productId2) {
                //so as to get descending list
                return productQuantityCount.get(productId2) - productQuantityCount.get(productId1);
            }
        });
        
        System.out.println("\n3 most popular products: ");
        for(int i=0; i<productList.size() && i<3; i++) {
            int id = productList.get(i);
            System.out.println(id + " quantity: " + productQuantityCount.get(id));
        }
    }
    
    public void getThreeBestCustomer() {
        final Map<Integer, Integer> customerRevenue = new HashMap<Integer, Integer>();
        Map<Integer, Order> orders = DataStore.getInstance().getOrders();
        Map<Integer, Product> products = DataStore.getInstance().getProducts();
        for(Order order : orders.values()) {
            int productId = order.getItem().getProductId();
            int min = products.get(productId).getMin();
            int selling = order.getItem().getSalesPrice();
            int quantity = order.getItem().getQuantity();
            int revenue = (selling - min) * quantity;
            int totalRevenue = 0;
            if(customerRevenue.containsKey(order.getCustomerId()))
                totalRevenue = customerRevenue.get(order.getCustomerId());
            totalRevenue += revenue;
            customerRevenue.put(order.getCustomerId(), totalRevenue);
        }
        
        List<Integer> customerList = new ArrayList<>(customerRevenue.keySet());
        Collections.sort(customerList, new Comparator<Integer>() {
            @Override
            public int compare(Integer customerId1, Integer customerId2) {
                //so as to get descending list
                return customerRevenue.get(customerId2) - customerRevenue.get(customerId1);
            }
        });
        
        System.out.println("\n3 best customers: ");
        for(int i=0; i<customerList.size() && i<3; i++) {
            int id = customerList.get(i);
            System.out.println(id + " revenue: " + customerRevenue.get(id));
        }
    }

    
    public void getTopThreeBestSalesPeople(){
        final Map<Integer, Integer> salesRevenue = new HashMap<Integer, Integer>();
        Map<Integer, Order> orders = DataStore.getInstance().getOrders();
        Map<Integer, Product> products = DataStore.getInstance().getProducts();
        for(Order order : orders.values()) {
            int productId = order.getItem().getProductId();
            int min = products.get(productId).getMin();
            int selling = order.getItem().getSalesPrice();
            int quantity = order.getItem().getQuantity();
            int revenue = (selling - min) * quantity;
            int totalRevenue = 0;
            if(salesRevenue.containsKey(order.getSalesId()))
                totalRevenue = salesRevenue.get(order.getSalesId());
            totalRevenue += revenue;
            salesRevenue.put(order.getSalesId(), totalRevenue);
        }
        List<Integer> salesList = new ArrayList<>(salesRevenue.keySet());
        Collections.sort(salesList, new Comparator<Integer>() {
            @Override
            public int compare(Integer salesId1, Integer salesId2) {
                return salesRevenue.get(salesId2) - salesRevenue.get(salesId1);
            }
        });
        
        System.out.println("\n3 best sales people: ");
        for(int i=0; i<salesList.size() && i<3; i++) {
            int id = salesList.get(i);
            System.out.println(id + " revenue: " + salesRevenue.get(id));
        }
    }
    
    public void getTotalRevenue() {
        int totalRevenue = 0;
        Map<Integer, Order> orders = DataStore.getInstance().getOrders();
        Map<Integer, Product> products = DataStore.getInstance().getProducts();
        for(Order order : orders.values()) {
            int productId = order.getItem().getProductId();
            int min = products.get(productId).getMin();
            int selling = order.getItem().getSalesPrice();
            int quantity = order.getItem().getQuantity();
            int revenue = (selling - min) * quantity;
            totalRevenue += revenue;
        }
        System.out.println("\nTotal revenue for the year: " + totalRevenue);
    }
        
}
