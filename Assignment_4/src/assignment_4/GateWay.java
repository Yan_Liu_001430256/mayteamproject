/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment_4;

import assignment_4.analytics.AnalysisHelper;
import assignment_4.analytics.DataStore;
import assignment_4.entities.Item;
import assignment_4.entities.Order;
import assignment_4.entities.Product;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author harshalneelkamal
 */
public class GateWay {
    
    DataReader orderReader;
    DataReader productReader;
    AnalysisHelper helper;
    
    public GateWay() throws IOException {
        DataGenerator generator = DataGenerator.getInstance();
        orderReader = new DataReader(generator.getOrderFilePath());
        productReader = new DataReader(generator.getProductCataloguePath());
        helper = new AnalysisHelper();
    }
    
    public static void main(String args[]) throws IOException{ 
        GateWay inst = new GateWay();
        inst.readData();
    }
        //Below is the sample for how you can use reader. you might wanna 
        //delete it once you understood.
    private void readData() throws IOException{
        String[] row;
        
        //printRow(orderReader.getFileHeader());
        while((row = orderReader.getNextRow()) != null){
            //printRow(orderRow);
            generateOrder(row);
        }
        while((row = productReader.getNextRow()) != null ){
            generateProduct(row);
        }
        runAnalysis();
    }
    
    private void generateProduct(String[] row){
        int productId = Integer.parseInt(row[0]);
        int min = Integer.parseInt(row[1]);
        int max = Integer.parseInt(row[2]);
        int target = Integer.parseInt(row[3]);
        Product p = new Product(min, max, target);
        DataStore.getInstance().getProducts().put(productId, p);
    }
    
    private void generateOrder(String[] row){
        int orderId = Integer.parseInt(row[0]);
        //int itemId = Integer.parseInt(row[1]);
        int productId = Integer.parseInt(row[2]);
        int quantity = Integer.parseInt(row[3]);
        int salesId = Integer.parseInt(row[4]);
        int customerId = Integer.parseInt(row[5]);
        int salesPrice = Integer.parseInt(row[6]);
        Item i = new Item(productId, salesPrice, quantity);
        Order o = new Order(orderId, salesId, customerId, i);
        DataStore.getInstance().getOrders().put(orderId, o);
    }
    
    private void runAnalysis(){
        helper.getThreeMostPopularProduct();
        helper.getThreeBestCustomer();
        helper.getTopThreeBestSalesPeople();
        helper.getTotalRevenue();
    }
        //DataReader productReader = new DataReader(generator.getProductCataloguePath());
        //String[] prodRow;
        //printRow(productReader.getFileHeader());
        //while((prodRow = productReader.getNextRow()) != null){
        //    printRow(prodRow);
        //}
    
    
    //public static void printRow(String[] row){
    //    for (String row1 : row) {
    //        System.out.print(row1 + ", ");
    //    }
    //    System.out.println("");
    //}
    
}
