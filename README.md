# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Course: INFO5100 Fall 2018
* Assignment: Lab 5
* Group: MAY
* Description: 
    * Extend Customer class provided in the skeleton from User class. Implement the abstract method in Customer class.
    * Add validation to the "AdminCreateScreen". Use Regex pattern matching to verify Username and Password.
        * Password: should contain alphanumeric characters with "+_$" being the allowed special characters.
        * Username: should be an email-ID with "_" and "@" as the only allowed special characters but should not start with an "_".
    * Complete the customer login screen.
    * Populate the Customer Table and ***** in the login screen should be replaced by text "Supplier" or "Customer" based on who is trying to log in. 

* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact