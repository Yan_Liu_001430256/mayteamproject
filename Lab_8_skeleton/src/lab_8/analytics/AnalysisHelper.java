/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab_8.analytics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import lab_8.entities.Comment;
import lab_8.entities.Post;
import lab_8.entities.User;

/**
 *
 * @author harshalneelkamal
 */
public class AnalysisHelper {
    
    
    public void userWithMostLikes(){
        Map<Integer , Integer> userLikecount = new HashMap<Integer, Integer>();
        
        Map<Integer, User>users = DataStore.getInstance().getUsers();
        for(User user : users.values()){
            for(Comment c : user.getComments()){
        int likes = 0;
        if(userLikecount.containsKey(user.getId()))
            likes = userLikecount.get(user.getId());
        likes += c.getLikes();
        userLikecount.put(user.getId(),likes);
        
    }
        
    }
        int max = 0;
        int maxId = 0;
        for(int id : userLikecount.keySet()){
            if(userLikecount.get(id)>max){
                max = userLikecount.get(id);
                maxId = id;
            }
        }
    System.out.println("User with most likes : "+max+"\n"+users.get(maxId));
    }
            
    public void getFiveMostLikedComment(){
        Map<Integer, Comment> comments = DataStore.getInstance().getComments();
        
        List<Comment> commentList = new ArrayList<>(comments.values());
        
        Collections.sort(commentList, new Comparator<Comment>(){
            @Override
            public int compare(Comment o1, Comment o2){
                //so as to get descending list
                return o2.getLikes() - o1.getLikes();
            }
        });
        System.out.println("5 most liked comments : ");
        for (int i = 0; i< commentList.size() && i<5;i++){
            System.out.println(commentList.get(i));
        }  
    }
    
    

    public void postWithMostComments(){
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        
        int max = 0;
        int maxId = 0;
        for(int id : posts.keySet()){
            if(posts.get(id).getComments().size()>max){
                max = posts.get(id).getComments().size();
                maxId = id;
            }
        }
        System.out.println("Post with most comments : " + max + "\n" + "Post id : " + maxId);
    }
    
    public void topFiveInactiveUsersBasedOnPosts(){
        
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        Map<Integer, User> users = DataStore.getInstance().getUsers();
        final Map<Integer, Integer> userIdWithPostCount = new HashMap<>();
        
        for(Post post : posts.values()) {
            int prevCount = 0;
            if(userIdWithPostCount.containsKey(post.getUserId()))
                prevCount = userIdWithPostCount.get(post.getUserId());
            prevCount += 1;
            userIdWithPostCount.put(post.getUserId(), prevCount);
        }

        List<Integer> userIds = new ArrayList<>(userIdWithPostCount.keySet());
        Collections.sort(userIds, new Comparator<Integer>(){
            @Override
            public int compare(Integer userId1, Integer userId2){
                //so as to get descending list
                return userIdWithPostCount.get(userId1) - userIdWithPostCount.get(userId2);
            }
        });

        System.out.println("Most 5 inactive users based on posts :");
        for(int i = 0;i<5;i++){
            int id = userIds.get(i);
            System.out.println(users.get(id) + " no. of posts: " + userIdWithPostCount.get(id) );
        }
        
    }
    
    public void postWithMostLikes(){
        
        Map<Integer, Integer> postLikecount = new HashMap<Integer, Integer>();
        
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        for(Post post : posts.values()) {
            for(Comment c : post.getComments()) {
                int likes = 0;
                if(postLikecount.containsKey(post.getPostId()))
                    likes = postLikecount.get(post.getPostId());
                likes += c.getLikes();
                postLikecount.put(post.getPostId(), likes);
            }
        }
        int max = 0;
        int maxId = 0;
        for(int id : postLikecount.keySet()) {
            if(postLikecount.get(id) > max) {
                max = postLikecount.get(id);
                maxId = id;
            }
        }
        System.out.println("Post with most likes: " + max + "\n" + "post id: " + maxId);
    }
    
    public void getFiveInactiveUserByComment(){
        Map<Integer, User> users = DataStore.getInstance().getUsers();
        
        List<User> userList = new ArrayList<>(users.values());
        
        Collections.sort(userList, new Comparator<User>() {
            @Override
            public int compare(User u1, User u2) {
                return u1.getComments().size() - u2.getComments().size();
            }
        });
        
        System.out.println("5 most inactive users based on comments: ");
        for(int i=0; i<userList.size() && i<5; i++) {
            System.out.println(userList.get(i));
        }
    }
    
    public void getAverageLikesPerComment(){
        Map<Integer, Comment> comments = DataStore.getInstance().getComments();
        int likes = 0;
        int length = comments.size();
        for(Comment c : comments.values()){
            int number = c.getLikes();
            likes = likes + number;
        }
        int averageNumber = likes/length;
        System.out.println("Average Number of likes:");
        System.out.println(averageNumber+"\n");
    }
    
    public void getTop5InactiveOverallUser(){
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        Map<Integer, User> users = DataStore.getInstance().getUsers();
        Map<Integer, Integer> user = new HashMap<Integer, Integer>();
        Map<Integer, Integer> us = new HashMap<Integer, Integer>();
        Map<Integer, Integer> u = new HashMap<Integer, Integer>();
        List<Post> postList = new ArrayList<>();
        int number = 0;
        int likes = 0;
        for(Post p:posts.values()){
            postList.add(p);
        }
        Collections.sort(postList, Post.compareByUserId());
        int maxId = postList.get(0).getUserId();
        
        for(int i=0; i<=maxId; i++){
            for(Post po:posts.values()){
                if(po.getUserId() == i){
                    number = number+1;
                }
            }
            
            user.put(i, number);
            number = 0;
        }
        
        for(Integer i:user.keySet()){
            number = user.get(i);
            number = number+users.get(i).getComments().size();
            us.put(i, number);
        }
        
        for(Integer integer: us.keySet()){
            for(Comment c: users.get(integer).getComments()){
                likes = likes + c.getLikes();
            }
            number = us.get(integer)+likes;
            u.put(integer, number);
            likes = 0;
        }

        Comparator<Map.Entry<Integer, Integer>> valueComparator = new Comparator<Map.Entry<Integer, Integer>>(){
            
            @Override
            public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2){
                return o1.getValue()-o2.getValue();
            }
        };
        List<Map.Entry<Integer, Integer>> list = new ArrayList<Map.Entry<Integer, Integer>>(u.entrySet());
        Collections.sort(list, valueComparator);
        System.out.println("Top5 Inactive Overall User:");
        System.out.println(users.get(list.get(0).getKey()));
        System.out.println(users.get(list.get(1).getKey()));
        System.out.println(users.get(list.get(2).getKey()));
        System.out.println(users.get(list.get(3).getKey()));
        System.out.println(users.get(list.get(4).getKey())+"\n");
    }
    
    public void getTop5ProactiveOverallUser(){
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        Map<Integer, User> users = DataStore.getInstance().getUsers();
        Map<Integer, Integer> user = new HashMap<Integer, Integer>();
        Map<Integer, Integer> us = new HashMap<Integer, Integer>();
        Map<Integer, Integer> u = new HashMap<Integer, Integer>();
        List<Post> postList = new ArrayList<>();
        int number = 0;
        int likes = 0;
        for(Post p:posts.values()){
            postList.add(p);
        }
        Collections.sort(postList, Post.compareByUserId());
        int maxId = postList.get(0).getUserId();
        
        for(int i=0; i<=maxId; i++){
            for(Post po:posts.values()){
                if(po.getUserId() == i){
                    number = number+1;
                }
            }
            
            user.put(i, number);
            number = 0;
        }
        
        for(Integer i:user.keySet()){
            number = user.get(i);
            number = number+users.get(i).getComments().size();
            us.put(i, number);
        }
        
        for(Integer integer: us.keySet()){
            for(Comment c: users.get(integer).getComments()){
                likes = likes + c.getLikes();
            }
            number = us.get(integer)+likes;
            u.put(integer, number);
            likes = 0;
        }
        
        Comparator<Map.Entry<Integer, Integer>> valueComparator = new Comparator<Map.Entry<Integer, Integer>>(){
            
            @Override
            public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2){
                return o2.getValue()-o1.getValue();
            }
        };
        List<Map.Entry<Integer, Integer>> list = new ArrayList<Map.Entry<Integer, Integer>>(u.entrySet());
        Collections.sort(list, valueComparator);
        System.out.println("Top5 Proactive Overall User:");
        System.out.println(users.get(list.get(0).getKey()));
        System.out.println(users.get(list.get(1).getKey()));
        System.out.println(users.get(list.get(2).getKey()));
        System.out.println(users.get(list.get(3).getKey()));
        System.out.println(users.get(list.get(4).getKey())+"\n");
    }
}